console.log("Hello World");

/*
	1. Create variables to store to the following user details:

	-first name - String
	-last name - String
	-age - Number
	-hobbies - Array
	-work address - Object

		-The hobbies array should contain at least 3 hobbies as Strings.
		-The work address object should contain the following key-value pairs:

			houseNumber: <value>
			street: <value>
			city: <value>
			state: <value>

	Log the values of each variable to follow/mimic the output.

	Note:
		-Name your own variables but follow the conventions and best practice in naming variables.
		-You may add your own values but keep the variable names and values Safe For Work.
*/

	//Add your variables and console log for objective 1 here:

// Activity Part 1

	let firstName = "Marc Carlo";
	console.log("First Name: " + firstName);

	let lastName = "Moratalla";
	console.log("Last Name: " + lastName);

	let aged = 32;
	console.log("Age: " + aged);

	console.log("Hobbies:");
	let hobbies = ["Cooking", "Playing Tennis", "Playing Video Games"];
	console.log(hobbies);

	console.log("Work Address:");
	let workAddress = {
			houseNumber: "32",
			street: "Washington",
			city: "Lincoln",
			state: "Nevraska"
	}
	console.log( workAddress)

	
/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/	


// Activity Part 2

	let fullName = 'Steve Rogers';
	console.log("Full Name is: " + fullName);

	let age = 40;
	console.log("Age is: " + 40);
	
	console.log("My Friends are:");
	let friends = ["Tony","Bruce","Thor","Natasha","Clint","Nick"];
	console.log(friends);
		
	console.log("My Full Profile:");
	let profile = {

		username: "captain_america",
		fullName: "Steve Rogers",
		age: 40,
		isActive: false
	}
	console.log(profile);

	let fullName2 = 'Bucky Barnes';
	console.log("My Beastfriend is: "  + fullName2);

	const lastLocation = "Arctic Ocean";
	console.log("I was found frozen in: " + lastLocation);
	
